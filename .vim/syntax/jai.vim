if exists("b:current_syntax")
  finish
endif

syntax keyword jaiUsing using
syntax keyword jaiCast cast
syntax keyword jaiTypeOf type_of
syntax keyword jaiSizeOf size_of
syntax keyword jaiTypeInfo type_info
syntax keyword jaiOperator operator

syntax keyword jaiStruct struct
syntax keyword jaiUnion union
syntax keyword jaiEnum enum enum_flags

syntax keyword jaiIf if
syntax match jaiStaticIf "#if"
syntax keyword jaiThen then
syntax keyword jaiElse else
syntax keyword jaiFor for
syntax keyword jaiWhile while
syntax keyword jaiBreak break
syntax keyword jaiContinue continue
syntax keyword jaiCase case

syntax keyword jaiDataType void string int float float32 float64 u8 u16 u32 u64 s8 s16 s32 s64 bool Any Type
syntax keyword jaiBool true false
syntax keyword jaiNull null
syntax match jaiUnitialized "---"

syntax keyword jaiReturn return
syntax keyword jaiDefer defer

syntax keyword jaiInline inline
syntax keyword jaiNoInline no_inline

syntax keyword jaiSOA SOA
syntax keyword jaiAOS AOS

syntax keyword jaiIt it
syntax keyword jaiItIndex it_index

syntax match jaiStringReplacement "%" contained display
syntax match jaiStringEscape "\\." contained display
syntax region jaiString start=/\v"/ skip=/\v\\./ end=/\v"/ contains=jaiStringReplacement, jaiStringEscape
syntax region jaiHereString matchgroup=jaiMacro start=/\v(#string\s+)@<=\z(\w+)/ skip=/\v\\./ end=/\v^<\z1>/ contains=jaiStringReplacement
syntax region jaiHereStringJai matchgroup=jaiMacro start=/\v(#string\s+)@<=\z(\w*JAI\w*)/ skip=/\v\\./ end=/\v^<\z1>/ contains=ALLBUT,jaiStringEscape,jaiEnumUnumbered
syntax match jaiEnumUnumbered "\v<\w+>\s*;@=" contained display
syntax region jaiEnumRegion start=/\v(<enum(_flags)?>\s+([us](8|16|32|64)|bool|int)?\s*)@<=\{/ skip=/\v\\.v/ end=/\v\}/ contains=ALLBUT,jaiStringReplacement,jaiStringEscape

syntax keyword jaiAutoCast xx

syntax match jaiTagNote "@\<\w\+\>" display

"syntax match jaiClass "\v<[A-Z]\w+>" display
"syntax match jaiConstant "\v<[A-Z0-9,_]+>" display

syntax match jaiDefineConstant "\v<\w+>(\s*:\s*\w*\s*:\s*)@="

syntax match jaiConstructorDestructor "\v#<(constructor|destructor)>"

syntax match jaiFunction "\v<\w+>(\s*::\s*((#\w+|(no_)?inline)?\s*\(|#bake(_values)?\s+\w+\s*\())@="
syntax match jaiOperatorFunction /\v(operator\s*)@<=[^:]+(::)@=/
"syntax match jaiDynamicFunction "\v<\w+(\s*:\=\s*\((.{-}:.{-})\))@="
syntax match jaiQuickLambda "\v<\w+>(\s*::.{-}\=\>)@="
"syntax match jaiDinamicLambda "\v<\w+(\s*:\=\s*.{-}\=\>)@="
"syntax match jaiFunctionCall "\v<\w+>(\()@="

syntax match jaiDefineType "\v<\w+>(\s*::\s*(#type|\*|\[.{-}\]|struct|union|enum|void|string|int|float(32|64)?|bool|[us](8|16|32|64)))@="

syntax match jaiInteger "\v<[0-9][0-9_]*>" display
"syntax match jaiFloat "\<[0-9][0-9_]*\%(\.[0-9][0-9_]*\)\%([eE][+-]\=[0-9_]\+\)\=" display
syntax match jaiFloat "\v((<([0-9][0-9_]*)>)?\.<[0-9][0-9_]*|<[0-9][0-9_]*>\.?)([eE][+-]?[0-9][0-9_]*)?>" display
syntax match jaiHexFloat "\v<0[hH]((_*[0-9a-fA-F]){8}|(_*[0-9a-fA-F]){16})_*>"
syntax match jaiHex "\v<0[xX][0-9A-Fa-f_]+>" display

syntax match jaiMacro "\v#<((no_)?inline|import|load|bake(_values)?|char|foreign(_system_library)?|type|string|modify|body_text|symmetric|place|(caller_)?location|complete|through|unshared|run|scope_(file|export)|dynamic_specialize)>" display

syntax match jaiTemplate "\v(\$\$?|\$\$\s+\$\$)(\s*\w+)@="

syntax match jaiCommentNote "@\<\w\+\>" contained display
syntax match jaiLineComment "//.*" contains=jaiCommentNote
syntax region jaiBlockComment start=/\v\/\*/ end=/\v\*\// contains=jaiBlockComment, jaiCommentNote

highlight link jaiIt Macro
highlight link jaiItIndex Macro
highlight link jaiUsing Keyword
highlight link jaiCast Keyword
highlight link jaiSizeOf Macro
highlight link jaiTypeOf Macro
highlight link jaiTypeInfo Macro
highlight link jaiOperator Keyword
highlight link jaiAutoCast Keyword
highlight link jaiReturn Keyword
highlight link jaiDefer Operator
highlight link jaiBreak Keyword
highlight link jaiContinue Keyword
highlight link jaiCase Keyword

highlight link jaiInline Keyword
highlight link jaiNoInline Keyword

highlight link jaiString String
highlight link jaiHereString String
highlight link jaiHereStringJai NONE
highlight link jaiEnumRegion NONE
highlight link jaiEnumUnumbered Constant
highlight link jaiStringReplacement Special
highlight link jaiStringEscape Special

highlight link jaiStruct Structure
highlight link jaiUnion Structure
highlight link jaiEnum Structure

highlight link jaiConstructorDestructor Function
highlight link jaiFunction Function
highlight link jaiOperatorFunction Function
"highlight link jaiDynamicFunction Function
highlight link jaiQuickLambda Function
"highlight link jaiDinamicLambda Function
"highlight link jaiFunctionCall Function

highlight link jaiMacro Macro
highlight link jaiIf Conditional
highlight link jaiStaticIf Conditional
highlight link jaiThen Conditional
highlight link jaiElse Conditional
highlight link jaiFor Repeat
highlight link jaiWhile Repeat

highlight link jaiLineComment Comment
highlight link jaiBlockComment Comment
highlight link jaiCommentNote Comment

"highlight link jaiClass Type
highlight link jaiDefineType Type
highlight link jaiDefineConstant Constant

highlight link jaiTemplate Macro

highlight link jaiTagNote Identifier
highlight link jaiDataType Type
highlight link jaiBool Boolean
highlight link jaiConstant Constant
highlight link jaiNull Constant
highlight link jaiUnitialized Constant
highlight link jaiInteger Number
highlight link jaiFloat Float
highlight link jaiHexFloat Float
highlight link jaiHex Number

highlight link jaiSOA Keyword
highlight link jaiAOS Keyword

let b:current_syntax = "jai"
