SHELL = /bin/bash
VIM_DETECT_FILES  = $(addprefix $(PWD)/.vim/ftdetect/,$(notdir $(wildcard $(HOME)/.vim/ftdetect/*)))
VIM_INDENT_FILES  = $(addprefix $(PWD)/.vim/indent/,$(notdir $(wildcard $(HOME)/.vim/indent/*)))
VIM_SYNTAX_FILES  = $(addprefix $(PWD)/.vim/syntax/,$(notdir $(wildcard $(HOME)/.vim/syntax/*)))
VIM_FILES = $(VIM_DETECT_FILES) $(VIM_INDENT_FILES) $(VIM_SYNTAX_FILES)
JAI_FILES  = $(addprefix $(PWD)/jai/,$(notdir $(wildcard $(HOME)/jai/*)))
HOME_FILES = $(addprefix $(PWD)/,$(notdir $(shell echo $(HOME)/{.bashrc,.vimrc,swap_capslock_and_esc})))

ALL_FILES  = .vim jai .bashrc .vimrc swap_capslock_and_esc .config

update: $(VIM_FILES) $(JAI_FILES) $(HOME_FILES)
	@echo files up-to-date

$(PWD)/.vim/ftdetect/%.vim: $(HOME)/.vim/ftdetect/%.vim
	cp $< $@
$(PWD)/.vim/indent/%.vim: $(HOME)/.vim/indent/%.vim
	cp $< $@
$(PWD)/.vim/syntax/%.vim: $(HOME)/.vim/syntax/%.vim
	cp $< $@
$(PWD)/jai/%.jai: $(HOME)/jai/%.jai
	cp $< $@
$(PWD)/.bashrc: $(HOME)/.bashrc
	cp $< $@
$(PWD)/.vimrc: $(HOME)/.vimrc
	cp $< $@
$(PWD)/swap_capslock_and_esc: $(HOME)/swap_capslock_and_esc
	cp $< $@

install:
	cp -r $(ALL_FILES) $(HOME)

