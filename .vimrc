ru! defaults.vim " use enhanced vim defaults
set scrolloff=0
set wildmode=list:longest,longest:full " better command line completion
set hidden " may change buffer while no saving the current one

" fix delay
set timeoutlen=1000
set ttimeoutlen=0

set tabstop=4 " the width of a tab is set to 4
set shiftwidth=4 " idents will have a width of 4
set softtabstop=4 " sets the number of columns of a tab
set noexpandtab " do not expand tab to spaces
set smartindent " smart ident
set relativenumber " relative line number
set hlsearch " highlight search
set ruler
set clipboard=unnamedplus
set backspace=2 " make backspace work like most other programs
set numberwidth=2

set background=light "fix colors
syntax enable " enable syntax highlight
filetype indent on
"autocmd BufEnter *.jai :syntax sync fromstart

"highlight Comment ctermfg=4
"highlight Constant ctermfg=1
"highlight Special ctermfg=5
"highlight Identifier ctermfg=6
"highlight Statement ctermfg=3
"highlight PreProc ctermfg=5
"highlight Type ctermfg=2

highlight LineNr ctermfg=darkblue
highlight Search ctermbg=red
highlight ErrorMsg ctermbg=yellow
highlight Error ctermbg=yellow
highlight MatchParen ctermfg=7
highlight SpellBad ctermfg=4
highlight ColorColumn ctermfg=4
highlight DiffDelete ctermfg=white
highlight Error ctermbg=3
highlight MatchParen ctermfg=white ctermbg=7

let mapleader = "\\" "leader is a *single* backslash
" make word uppercase in insert mode
inoremap <c-u> <esc>mU~gUb`Ua
nnoremap K K<cr>
nnoremap \c :make<cr><cr>
nnoremap \d :make debug<cr><cr>
nnoremap \t :make tags<cr><cr>
" count number of occurrences of last search
nnoremap \n :%s///gn<cr>
" open .vimrc in a split window
nnoremap <leader>ev :split $MYVIMRC<cr>
" update vim from .vimrc
nnoremap <leader>sv :source $MYVIMRC<cr>

set tags+=~/.vim/tags/**/tags
